module "gitlab_runner" {
  source  = "gitlab.com/vigigloo/tf-modules/k8sgitlabrunner"
  version = "0.2.3"

  kubeconfig    = data.scaleway_k8s_cluster.dev.kubeconfig[0]
  project_slug  = var.project_slug
  project_name  = var.project_name
  gitlab_groups = []
}
