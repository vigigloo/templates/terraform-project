# Random generation for secret org id
resource "random_string" "production_secret_org_id" {
  special = false
  length  = 63
}
resource "random_string" "development_secret_org_id" {
  special = false
  length  = 63
}

# Add loki datasource
resource "grafana_data_source" "production_loki" {
  provider = grafana.production
  name     = "loki"
  type     = "loki"
  url      = var.grafana_production_loki_url

  http_headers = {
    X-Scope-OrgID = random_string.production_secret_org_id.result
  }
}

# Add loki datasource
resource "grafana_data_source" "development_loki" {
  provider = grafana.development
  name     = "loki"
  type     = "loki"
  url      = var.grafana_development_loki_url

  http_headers = {
    X-Scope-OrgID = random_string.development_secret_org_id.result
  }
}

resource "grafana_data_source" "production_prometheus" {
  provider = grafana.production
  name     = "mimir"
  type     = "prometheus"
  url      = var.grafana_production_prometheus_url

  http_headers = {
    X-Scope-OrgID = random_string.production_secret_org_id.result
  }
}

resource "grafana_data_source" "development_prometheus" {
  provider = grafana.development
  name     = "mimir"
  type     = "prometheus"
  url      = var.grafana_development_prometheus_url

  http_headers = {
    X-Scope-OrgID = random_string.development_secret_org_id.result
  }
}

resource "grafana_data_source" "production_prometheus_cluster" {
  provider = grafana.production
  name     = "mimir-cluster"
  type     = "prometheus"
  url      = var.grafana_production_prometheus_url

  http_headers = {
    X-Scope-OrgID = "default"
  }
}

resource "grafana_data_source" "development_prometheus_cluster" {
  provider = grafana.development
  name     = "mimir-cluster"
  type     = "prometheus"
  url      = var.grafana_development_prometheus_url

  http_headers = {
    X-Scope-OrgID = "default"
  }
}
